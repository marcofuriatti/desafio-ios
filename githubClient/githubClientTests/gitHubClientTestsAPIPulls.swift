//
//  gitHubClientTestsAPIPulls.swift
//  githubClient
//
//  Created by Marco Furiatti on 19/03/17.
//  Copyright © 2017 Furiatti. All rights reserved.
//

import XCTest
@testable import githubClient

class gitHubClientTestsAPIPulls: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testList() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let expec = expectation(description: "GET testList")

        APIPulls.sharedInstance.list(fullName: "ReactiveX/RxJava") { (pulls) in
            
            XCTAssertNotEqual(pulls.count, 0)
            
            expec.fulfill()
        }
        
        waitForExpectations(timeout: Constants.kTimeout) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
