//
//  PullsTableViewController.swift
//  githubClient
//
//  Created by Marco Furiatti on 19/03/17.
//  Copyright © 2017 Furiatti. All rights reserved.
//

import UIKit

class PullsTableViewController: ListTableViewController {
    
    var repository: Repository = Repository()
    var pulls: [Pull] = [Pull]() {
        didSet{
            total.text = String(format: NSLocalizedString("pulls.list.total", comment:""), pulls.count)
        }
    }
    
    @IBOutlet weak var total: UILabel!
    
    override func viewDidLoad() {
        loadEfect = false
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        title = repository.name
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func loadData() {
        super.loadData()
        APIPulls.sharedInstance.list(fullName: repository.full_name) { (pulls) in
   
            self.pulls = pulls
            self.tableView.reloadData()
            
            UIView.animate(withDuration: Constants.timeAnimation.fast) {
                self.tableView.alpha = 1
            }
        }
    }
    
    // MARK: - TableView dataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pulls.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: PullsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "pullCell", for: indexPath) as? PullsTableViewCell {
            
            let pull: Pull = pulls[indexPath.row]
            cell.setData(pull: pull)
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        if let pull = pulls[indexPath.row] as Pull? {
            if UIApplication.shared.canOpenURL(URL(string: pull.html_url)!) {
                UIApplication.shared.openURL(URL(string: pull.html_url)!)
            }
        }
    }
    // MARK: - TableView dataSource End
}

