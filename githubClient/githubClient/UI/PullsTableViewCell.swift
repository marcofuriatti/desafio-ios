//
//  PullsTableViewCell.swift
//  githubClient
//
//  Created by Marco Furiatti on 20/03/17.
//  Copyright © 2017 Furiatti. All rights reserved.
//

import UIKit
import SDWebImage

class PullsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var userLogin: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var dataCreated: UILabel!

    func setData(pull: Pull) {
   
        userImage.sd_setImage(with: URL(string: pull.user.avatar_url))
        title.text = pull.title
        userLogin.text = pull.user.login
        body.text = pull.body
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
      
        if let date = formatter.date(from: pull.created_at) {
            formatter.dateFormat = NSLocalizedString("date.format", comment:"")
            dataCreated.text = formatter.string(from: date)
        }
    }
}
