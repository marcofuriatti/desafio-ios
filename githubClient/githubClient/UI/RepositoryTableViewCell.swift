//
//  RepositoriesTableViewCell.swift
//  githubClient
//
//  Created by Marco Furiatti on 19/03/17.
//  Copyright © 2017 Furiatti. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryTableViewCell: UITableViewCell {

    @IBOutlet weak var ownerImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var ownerUser: UILabel!
    @IBOutlet weak var stars: UILabel!
    @IBOutlet weak var forks: UILabel!

    func setData(repository: Repository) {
   
        ownerImage.sd_setImage(with: URL(string: repository.owner.avatar_url))
        name.text = repository.name
        desc.text = repository.description
        ownerUser.text = repository.owner.login
        stars.text = String(Int(repository.stargazers_count))
        forks.text = String(Int(repository.forks_count))
    }
}
