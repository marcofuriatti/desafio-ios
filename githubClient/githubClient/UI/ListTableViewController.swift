//
//  ListTableViewController.swift
//  githubClient
//
//  Created by Marco Furiatti on 19/03/17.
//  Copyright © 2017 Furiatti. All rights reserved.
//

import UIKit

class ListTableViewController: UITableViewController {
    
    var page: Int = 0
    var loadEfect: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Methods
    
    func loadData() {
        if loadEfect {
            UIView.animate(withDuration: Constants.timeAnimation.fast) {
                self.tableView.alpha = 0
            }
        }
    }
    //MARK: - Methods End
}
