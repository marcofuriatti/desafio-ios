//
//  RepositoryTableViewController.swift
//  githubClient
//
//  Created by Marco Furiatti on 19/03/17.
//  Copyright © 2017 Furiatti. All rights reserved.
//

import UIKit

class RepositoryTableViewController: ListTableViewController {

    var repositories: Repositories = Repositories()
    var repository: Repository = Repository()
    var language: Language = Language.Java {
        didSet{
            page = 0
            changeTitle()
            self.loadEfect = true
            loadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        changeTitle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func loadData() {
        super.loadData()
        
        APIRepositories.sharedInstance.list(language: language, sort: Sort.Stars, page: page) { (repositories) in
   
            if self.page == 0 {
                self.repositories = repositories
            } else {
                self.repositories.items = self.repositories.items + repositories.items
                self.repositories.total_count = repositories.total_count
            }
            self.page = self.page + 1
            self.tableView.reloadData()
            
            if self.loadEfect {
                UIView.animate(withDuration: Constants.timeAnimation.fast) {
                    self.tableView.alpha = 1
                }
            }
            self.loadEfect = false
        }
    }
    
    func changeTitle() {
        title = "GitHub " + language.rawValue + " Pop"
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is PullsTableViewController {
            (segue.destination as! PullsTableViewController).repository = repository
        }
    }
    
    // MARK: - TableView dataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        if let cell: RepositoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "repositoryCell", for: indexPath) as? RepositoryTableViewCell {
            let repository: Repository = repositories.items[indexPath.row]
            cell.setData(repository: repository)
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
 
        if repositories.items.last != nil && indexPath.row == repositories.items.count - 1 {
            if repositories.total_count > repositories.items.count {
                loadData()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        repository = repositories.items[indexPath.row]
        performSegue(withIdentifier: "showPull", sender: nil)
    }
    // MARK: - TableView dataSource End
    

    @IBAction func filterAction(_ sender: Any) {
    
        let optionFilter : UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            optionFilter.modalPresentationStyle = .popover
            optionFilter.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        }
        
        let csharp : UIAlertAction = UIAlertAction(title: Language.CSharp.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.language = .CSharp
        })
        optionFilter.addAction(csharp)
        
       let java : UIAlertAction = UIAlertAction(title: Language.Java.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.language = .Java
        })
        optionFilter.addAction(java)
        
        let js : UIAlertAction = UIAlertAction(title: Language.JavaScript.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.language = .JavaScript
        })
        optionFilter.addAction(js)
        
        let objc : UIAlertAction = UIAlertAction(title: Language.ObjectiveC.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.language = .ObjectiveC
        })
        optionFilter.addAction(objc)
        
        let swift : UIAlertAction = UIAlertAction(title: Language.Swift.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.language = .Swift
        })
        optionFilter.addAction(swift)
        
        let cancel : UIAlertAction = UIAlertAction(title: NSLocalizedString("option.cancel", comment:""), style: .destructive, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        optionFilter.addAction(cancel)

        
        self.present(optionFilter, animated: true, completion: nil)
    }
}

