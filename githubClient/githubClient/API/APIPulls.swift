//
//  APIPulls.swift
//  githubClient
//
//  Created by Marco Furiatti on 19/03/17.
//  Copyright © 2017 Furiatti. All rights reserved.
//

import Alamofire

public class APIPulls: APIClient {
    
    public class var sharedInstance : APIPulls {
        struct Static {
            static let instance : APIPulls = APIPulls()
        }
        return Static.instance
    }
    
    func list(fullName : String, callback : @escaping (_ pulls : [Pull]) -> Void) {
        let url = String(format: Constants.Url.Pulls.kPulls, fullName)
   
        Alamofire.request(url).responseString { (response) in
            if let jsonString = response.result.value {
                if let pulls = [Pull].deserialize(from: jsonString) {
                    callback(pulls as! [Pull])
                } else {
                    callback([Pull]())
                }
            }
        }
    }
}

