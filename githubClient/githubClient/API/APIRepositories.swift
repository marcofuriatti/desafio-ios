//
//  APIRepositories.swift
//  githubClient
//
//  Created by Marco Furiatti on 19/03/17.
//  Copyright © 2017 Furiatti. All rights reserved.
//

import Alamofire

public enum Language : String {
    case CSharp = "CSharp"
    case Java = "Java"
    case JavaScript = "JS"
    case ObjectiveC = "ObjectiveC"
    case Swift = "Swift"
}

public enum Sort : String {
    case Stars = "Stars"
}

public class APIRepositories: APIClient {

    public class var sharedInstance : APIRepositories {
        struct Static {
            static let instance : APIRepositories = APIRepositories()
        }
        return Static.instance
    }
    
    func list(language : Language, sort : Sort, page : Int, callback : @escaping (_ repositories : Repositories) -> Void) {
        let url = String(format: Constants.Url.Search.kRepositories, language.rawValue, sort.rawValue, page)
        
        Alamofire.request(url).responseString { (response) in
            
            if let jsonString = response.result.value {
                let repositories = Repositories.deserialize(from: jsonString)
                
                callback(repositories != nil ? repositories! : Repositories())
            }
        }
    }
}
