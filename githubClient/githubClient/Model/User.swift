//
//  User.swift
//  githubClient
//
//  Created by Marco Furiatti on 19/03/17.
//  Copyright © 2017 Furiatti. All rights reserved.
//

import HandyJSON

class User : HandyJSON {
    
    var login: String = ""
    var name: String = ""
    var id: Double = 0
    var avatar_url: String = ""
    var gravatar_id: String = ""
    var url: String = ""
    var html_url: String = ""
    var followers_url: String = ""
    var following_url: String = ""
    var gists_url: String = ""
    var starred_url: String = ""
    var subscriptions_url: String = ""
    var organizations_url: String = ""
    var repos_url: String = ""
    var events_url: String = ""
    var received_events_url: String = ""
    var type: String = ""
    var site_admin: Bool = false
    
    required init() {}
}

