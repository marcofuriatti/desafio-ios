//
//  Repositories.swift
//  githubClient
//
//  Created by Marco Furiatti on 19/03/17.
//  Copyright © 2017 Furiatti. All rights reserved.
//

import HandyJSON

class Repositories: HandyJSON {
    
    var total_count: Int = 0
    var incomplete_results: Bool = false
    var items: [Repository] = [Repository]()
    
    required init() {}
}
