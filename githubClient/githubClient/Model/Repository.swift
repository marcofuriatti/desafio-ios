//
//  Repository.swift
//  githubClient
//
//  Created by Marco Furiatti on 19/03/17.
//  Copyright © 2017 Furiatti. All rights reserved.
//

import HandyJSON

class Repository : HandyJSON {
    
    var id: Double = 0
    var name: String = ""
    var full_name: String = ""
    var owner: User = User()
    var priv: Bool = false
    var html_url: String = ""
    var description: String = ""
    var fork: String = ""
    var url: String = ""
    var forks_url: String = ""
    var keys_url: String = ""
    var collaborators_url: String = ""
    var teams_url: String = ""
    var hooks_url: String = ""
    var issue_events_url: String = ""
    var events_url: String = ""
    var assignees_url: String = ""
    var branches_url: String = ""
    var tags_url: String = ""
    var blobs_url: String = ""
    var git_tags_url: String = ""
    var git_refs_url: String = ""
    var trees_url: String = ""
    var statuses_url: String = ""
    var languages_url: String = ""
    var stargazers_url: String = ""
    var contributors_url: String = ""
    var subscribers_url: String = ""
    var subscription_url: String = ""
    var commits_url: String = ""
    var git_commits_url: String = ""
    var comments_url: String = ""
    var issue_comment_url: String = ""
    var contents_url: String = ""
    var compare_url: String = ""
    var merges_url: String = ""
    var archive_url: String = ""
    var downloads_url: String = ""
    var issues_url: String = ""
    var pulls_url: String = ""
    var milestones_url: String = ""
    var notifications_url: String = ""
    var labels_url: String = ""
    var releases_url: String = ""
    var deployments_url: String = ""
    var created_at: String = ""
    var updated_at: String = ""
    var pushed_at: String = ""
    var git_url: String = ""
    var ssh_url: String = ""
    var clone_url: String = ""
    var svn_url: String = ""
    var homepage: String = ""
    var size: Double = 0
    var stargazers_count: Double = 0
    var watchers_count: Double = 0
    var language: Double = 0
    var has_issues: Bool = false
    var has_downloads: Bool = false
    var has_wiki: Bool = false
    var has_pages: Bool = false
    var forks_count: Double = 0
    var mirror_url: String = ""
    var open_issues_count: Double = 0
    var forks: Double = 0
    var open_issues: Double = 0
    var watchers: Double = 0
    var default_branch: String = ""
    var score: Double = 0

    required init() {}
    
    func mapping(mapper: HelpingMapper) {
        
        mapper <<<
        self.priv <-- "private"
    }
}
