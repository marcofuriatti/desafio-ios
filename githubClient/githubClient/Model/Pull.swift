//
//  Pull.swift
//  githubClient
//
//  Created by Marco Furiatti on 19/03/17.
//  Copyright © 2017 Furiatti. All rights reserved.
//

import HandyJSON

class Pull : HandyJSON {
    
    var html_url: String = ""
    var url: String = ""
    var user: User = User()
    var body: String = ""
    var title: String = ""
    var created_at: String = ""
    
    required init() {}
}
