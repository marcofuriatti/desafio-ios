//
//  Constants.swift
//  githubClient
//
//  Created by Marco Furiatti on 19/03/17.
//  Copyright © 2017 Furiatti. All rights reserved.
//

import Foundation

struct Constants {
    static let kTimeout : Double = 30.0
    
    struct timeAnimation {
        static let fast = 0.25
    }
    
    struct Url {
        static let kHttps = "https://"
        static let kBaseUrl = kHttps + "api.github.com"
        
        struct ParamsType {
            static let kLanguage = "language:"
            static let kSort = "sort:"
            static let kPage = "page:"
        }
        
        struct Search {
            static let kUrl = kBaseUrl + "/search"
            static let kRepositories = kUrl + "/repositories?q=" + ParamsType.kLanguage + "%@&" + ParamsType.kSort + "%@&" + ParamsType.kPage + "%i"
        }
        
        struct Pulls {
            static let kUrl = kBaseUrl + "/repos"
            static let kPulls = kUrl + "/%@/pulls"
        }
    }
}

